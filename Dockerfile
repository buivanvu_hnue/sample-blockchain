FROM node:10.11.0

WORKDIR /usr/src/app
COPY ./package.json ./package.json
COPY . .
RUN npm install
# RUN npm install -g nodemon
ENV NODE_ENV development_docker
# ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["node", "index.js"]
