#!/bin/bash
set -e
npm run migrate

if [$NODE_ENV == 'development']; then
  echo 'DEVELOP ENV'
  exec nodemon index.js
else
  exec npm run start
  echo 'prod ENV'
fi