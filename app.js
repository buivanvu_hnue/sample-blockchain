/**
 * Simple express app
 */
import express from 'express'
import { Server } from 'http'
import defaultRouter from './route'
import config from './config'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import Knex from 'knex'
import session from 'express-session'
import passport from 'passport'
import { InitPassPort } from './core/Factory'


const knex = Knex(require('./knexfile'))
const KnexSessionStore = require('connect-session-knex')(session)
const store = new KnexSessionStore({knex})

InitPassPort(passport)

let app = express()
let http = Server(app)

global.appConfig = config

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
app.set('port', 3003)
app.use('/public', express.static('public'))
app.set('view engine', 'ejs')

app.use(session({
  secret: 'artofcode',
  cookie: {
    maxAge: 846000000
  },
  store,
  resave: false,
  saveUninitialized: true
}))

app.use(passport.initialize())
app.use(passport.session())
app.use('/', defaultRouter)


http.listen(app.get('port'), function () {
  console.log('API Server listen at: ' + app.get('port'))
})
