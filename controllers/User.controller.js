
import User from '../models/User'
import passport from 'passport'

export const getUserList = async (req, res) => {
    const data = await User.query()
    return res.json({
      data,
      count: data.length
    })
}

export const getSellerList = async (req, res) => {
  const data = await User.query().where({role: "Seller"}).select(['id', 'fullName', 'account_address'])
  return res.json(data)
}

export const registerNewUser = async (req, res) => {
  try {
    let userData = {...req.body, passwordHash: req.body.password}
    delete userData.password
    delete userData.address
    let user = await User.query().insert(userData)
    return res.json(user)
  } catch (err) {
    console.log(err)
    return res.status(500).end(err.message)
  }
}

export const updateUser = async (req, res) => {
  try {
    let userData = req.body.user
    if (userData.passwordHash === '' || userData.passwordHash === null) {
      delete userData.passwordHash
    }
    console.log(userData)
    let user = await User.query().findById(userData.id || req.params.id)
      .patch(userData)
    return res.json(userData)

  } catch (err) {
    console.log(err)
    return res.status(500).end(err.message)
  }
}

export const setCookies = (req, res) => {
  res.cookie('sid', Math.random() + 'cookie', {expire: 360000 + Date.now()})
  req.session.abc = 'xyz'
  res.end('Cookie already set')
}

export const login = (req, res, next) => {
  passport.authenticate('local', { session: true }, (err, user, info) => {
    if (err || !user) {
      console.log(info)
      return res.status(202).json({
        'status': 'LOGIN_FAILED',
        'message': info && info.message || 'Login failed'
      })
    }
    req.login(user, { session: true }, async err => {
      if (err) {
        res.status(400).json({
          'status': 'LOGIN_FAILED',
          'message': err && err.message
        })
      }
      return res.json({ user })
    })
  })(req, res, next)
}

export const getSessionData = async (req, res) => {
  let user = {}
  if (req.user) {
    user = await User.query().findById(req.user.id)
  }
  res.json({user, data: req.session})
}
