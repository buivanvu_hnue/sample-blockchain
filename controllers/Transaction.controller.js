import Web3 from 'web3'
import TransactionModel from '../models/Transaction.js'
import User from '../models/User'

const Tx = require('ethereumjs-tx').Transaction
const config = require("../config")

const contractObject =  require('../contract/Marketplace.compiled.json')
const web3 = new Web3(config.eth_http_provider)

// Don't known why
if (process.env.NODE_ENV == "development_docker") {
  const eventProvider = new Web3.providers.WebsocketProvider(
    "ws://ganache:8545"
  );
  web3.setProvider(eventProvider);
  console.log("SET WS provider")
}

const getUserContractTransaction = async function(idUser) {
  return await TransactionModel.query().findOne({
    id_user: idUser,
    type: 'deploy_contract'
  })
}

const getPrivateKey = async function(userId) {
  const user = await User.query().findById(userId)
  if (!user) {
    throw new Error("User not found")
  }
  return Buffer.from(user.account_private_key, 'hex')
}

export const getAllAccount = async (req, res) => {
  const accounts = await web3.eth.getAccounts()
  let accountsStat = {}
  for (let account of accounts) {
    const balanceWei = await web3.eth.getBalance(account)
    accountsStat[account] = web3.utils.fromWei(balanceWei, 'ether')
  }
  return res.json(accountsStat);
}

export const getAccountBalance = async (req, res) => {
  if (!req.user || !req.user.account_address) {
    res.json({
      eth: 0
    })
  }
  const balanceWei = await web3.eth.getBalance(req.user.account_address)
  const eth = web3.utils.fromWei(balanceWei, 'ether')
  res.json({eth, account_address: req.user.account_address})
}

export const deploySellerContract = async (req, res) => {
  if (!req.user.id) {
    return res.status(400).json({"message": "please login!"})
  }
  const user = await User.query().findById(req.user.id)
  if (!user || user.role !== 'Seller' ) {
    return res.status(400).json({
      "status": "error",
      "message": "You are not a seller!"
    })
  }

  const accountAddress = user.account_address
  const privateKey = Buffer.from(user.account_private_key, 'hex')
  // Deploy the contract
  web3.eth.getTransactionCount(accountAddress, (err, txCount) => {
    const txObject = {
      nonce:    web3.utils.toHex(txCount),
      gasLimit: web3.utils.toHex(1000000), 
      gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei')),
      data: contractObject.byteCode.object
    }

    const tx = new Tx(txObject)
    tx.sign(privateKey)

    const serializedTx = tx.serialize()
    const raw = '0x' + serializedTx.toString('hex')

    web3.eth.sendSignedTransaction(raw, (err, txHash) => {
      // console.log('err:', err, 'txHash:', txHash)
      TransactionModel.query().insert({
        txCount,
        txHash,
        address: accountAddress,
        id_user: user.id,
        type: "deploy_contract"
      }).then(console.log)
     })
    .on('receipt', async (record) => {
      if (record) {
        const transaction = await TransactionModel.query()
        .update({
          contractAddress: record.contractAddress,
          gas: record.gasUsed
        })
        .where({
          txHash: record.transactionHash
        })
        console.log(transaction, 'Saved transaction')
      }
      res.json(record)
    })
    .on('error', err => {
      console.log(err)
      res.status(500).end(err.message)
    })

  })
}


export const getProductList = async(req, res) => {
  let userId = req.query.userId
  let user = req.user || null
  if (userId) {
    user = await User.query().findById(userId)
  }

  let products = []
  if (user && user.role === 'Seller') {
    let contractTrans = await getUserContractTransaction(user.id)
    if (contractTrans && contractTrans.contractAddress) {
      const marketContract = new web3.eth.Contract(contractObject.abi, contractTrans.contractAddress)
      const productCount = await marketContract.methods.productCount().call()
      for (var i = 1; i <= productCount; i++) {
        const product = await marketContract.methods.products(i).call()
        products.push(product)
      }
      return res.json(products)
    }
  }
  return res.json(products)
}

export const addProduct = async (req, res) => {
  const { user } = req
  const { name, price } = req.body
  if (user && user.role === 'Seller') {
    let contractTrans = await getUserContractTransaction(user.id)
    const privateKey = await getPrivateKey(user.id)
    const accountAddress = user.account_address
    if (contractTrans && contractTrans.contractAddress) {
      const contract = new web3.eth.Contract(contractObject.abi, contractTrans.contractAddress)
      const txCount = await web3.eth.getTransactionCount(user.account_address)
      const txObject = {
        nonce:    web3.utils.toHex(txCount),
        gasLimit: web3.utils.toHex(800000),
        gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei')),
        to: contractTrans.contractAddress,
        data: contract.methods.createProduct(name, price).encodeABI()
      }
    
      const tx = new Tx(txObject)
      tx.sign(privateKey)
      const serializedTx = tx.serialize()
      const raw = '0x' + serializedTx.toString('hex')
      web3.eth.sendSignedTransaction(raw, (err, txHash) => {
        if (err) {
          res.status(500).end(err.message)
        }
        TransactionModel.query().insert({
          txCount,
          txHash,
          address: accountAddress,
          id_user: user.id,
          type: "create_product"
        }).then(console.log)
       })
      .on('receipt', async (record) => {
        if (record) {
          const transaction = await TransactionModel.query()
          .update({
            gas: record.gasUsed
          })
          .where({
            txHash: record.transactionHash
          })
          console.log(transaction, 'Saved transaction')
          res.json(record)
        }
      })
      .on('error', err => {
        res.status(500).end(err.message)
      })
      
    }
  } else {
    res.status(500).end('not a seller')
  }
}


export const buyProduct = async (req, res) => {
  const { user } = req
  const { owner, price, product, seller} = req.body
  console.log(req.body)

  if (user && user.role === 'Customer') {
    let contractTrans = await getUserContractTransaction(seller)
    const privateKey = await getPrivateKey(user.id)
    const accountAddress = user.account_address
    if (contractTrans && contractTrans.contractAddress) {
      const contract = new web3.eth.Contract(contractObject.abi, contractTrans.contractAddress)
      const txCount = await web3.eth.getTransactionCount(user.account_address)
      const txObject = {
        nonce:    web3.utils.toHex(txCount),
        gasLimit: web3.utils.toHex(900000), // Block gas limit
        gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei')),
        to: contractTrans.contractAddress,
        value: web3.utils.toHex(web3.utils.toWei(price, 'ether')),
        data: contract.methods.purchaseProduct(product).encodeABI()
      }
    
      const tx = new Tx(txObject)
      tx.sign(privateKey)
      const serializedTx = tx.serialize()
      const raw = '0x' + serializedTx.toString('hex')
      web3.eth.sendSignedTransaction(raw, (err, txHash) => {
        if (err) {
          res.status(500).end(err.message)
        }
        TransactionModel.query().insert({
          txCount,
          txHash,
          address: accountAddress,
          id_user: user.id,
          type: "buy_product"
        }).then(console.log)
       })
      .on('receipt', async (record) => {
        if (record) {
          const transaction = await TransactionModel.query()
          .update({
            gas: record.gasUsed
          })
          .where({
            txHash: record.transactionHash
          })
          console.log(transaction, 'Saved transaction')
          res.json(record)
        }
      })
      .on('error', err => {
        res.status(500).end(err.message)
      })
      
    }
  } else {
    res.status(500).end('not a customer')
  }
}