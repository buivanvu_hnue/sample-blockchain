exports.up = async function(knex, Promise) {
  await Promise.all([
    knex.schema.createTable('transactions', function(table) {
      table.uuid('id').primary()
      table.uuid('id_user') // admin or user
      table.string('type')
      table.string('txHash')
      table.string('txCount')
      table.string('address')
      table.string('to_address')
      table.string('amount_eth')
      table.string('gas')
      table.text('params')
      table.dateTime('createdAt').nullable()
      table.dateTime('disableAt').nullable()
      table.timestamp('updatedAt')
      //Index
      table.index('txHash')
      table.index('id_user')
    })
  ])
}

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('transactions')
  ])
}
