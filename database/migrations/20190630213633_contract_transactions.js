
exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('transactions', function (table) {
      table.string('contractAddress').default("")
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('transactions', function (table) {
      table.dropColumn('contractAddress')
    })
  ])
}

