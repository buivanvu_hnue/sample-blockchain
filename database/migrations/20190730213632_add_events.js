exports.up = async function(knex, Promise) {
  await Promise.all([
    knex.schema.createTable('events', function(table) {
      table.uuid('id').primary()
      table.string('eventName')
      table.string('transactionHash')
      table.string('contractAddress')
      table.integer('blockNumber')
      table.string('blockHash')
      table.string('address')
      table.string('amount_eth')
      table.string('type')
      table.string('event_id')
      table.text('info', 'mediumtext')
      table.dateTime('createdAt').nullable()
      table.dateTime('disableAt').nullable()
      table.timestamp('updatedAt')

      //Index
      table.index('contractAddress')
      table.index('blockNumber')
      table.index('eventName')
      table.index('event_id')
      table.index('transactionHash')
      table.index(['contractAddress', 'blockNumber'])

      table.unique(['contractAddress', 'event_id', 'eventName'])
    })
  ])
}

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('events')
  ])
}
