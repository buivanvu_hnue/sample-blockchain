exports.up = async function(knex, Promise) {
  await Promise.all([
    knex.schema.createTable('users', function(table) {
      table.uuid('id').primary()
      table.string('role') // admin or user
      table.string('fullName')
      table.string('email').unique()
      table.string('username').unique()
      table.string('passwordHash')
      table.string('account_address')
      table.string('account_private_key')
      table.string('account_balance')
      table.dateTime('createdAt').nullable()
      table.dateTime('disableAt').nullable()
      table.timestamp('updatedAt')
      //Index
      table.index('email')
    })
  ])
}

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('users')
  ])
}
