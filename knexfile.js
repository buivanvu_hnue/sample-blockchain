const config = require('./config')
module.exports = Object.assign(
  {
    migrations: {
      directory: './database/migrations'
    },
    seeds: {
      directory: './database/seeds'
    },
    useNullAsDefault: true
  },
  config.mysql
)
console.log(config)