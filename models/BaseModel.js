const guid = require('objection-guid')()
const Model = guid(require('objection').Model)
export const knex = require('knex')(require('../knexfile'))

Model.knex(knex)

class ModelBase extends guid(Model) {
  $beforeUpdate(context) {
    const parent = super.$beforeInsert(context)
    return Promise.resolve(parent).then(() => (this.updatedAt = new Date()))
  }

  $beforeInsert(context) {
    const parent = super.$beforeInsert(context)
    return Promise.resolve(parent).then(() => (this.createdAt = new Date()))
  }
  // $formatJson(json, options) {
  //   json = super.$formatJson(json, options)
  //   Object.keys(this.$secureFields).forEach(key => delete json[key])
  //   return json
  // }
}
export default ModelBase
