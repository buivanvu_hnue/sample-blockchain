'use strict'
import ObjectionPassword from 'objection-password'
import ModelBase from './BaseModel'

const Password = ObjectionPassword({ passwordField: 'passwordHash' })

export default class UserModel extends Password(ModelBase) {
  static get tableName() {
    return 'users'
  }
  static get virtualAttributes() {
    return ['fullName']
  }
  get $secureFields() {
    return ['passwordHash']
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['email', 'fullName', 'account_address', 'account_private_key'],
      properties: {
        id: { type: 'uuid' },
        fullName: { type: 'string', minLength: 1, maxLength: 255 },
        email: { type: 'string' },
        role: { type: 'string' },
        account_address: { type: 'string' },
        account_private_key: { type: 'string' },
      }
    }
  }
}

