'use strict'
import ModelBase from './BaseModel'



export default class TransactionModel extends ModelBase {
  static get tableName() {
    return 'transactions'
  }

  static get jsonSchema() {
    return {
      type: 'object',
      properties: {
        id: { type: 'uuid' },
        params: { type: 'object' }
      }
    }
  }
}

