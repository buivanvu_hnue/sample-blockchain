import router from './core/Router'
import { getAllAccount } from './controllers/Transaction.controller'
import { registerNewUser, login, getSessionData, getUserList, getSellerList } from './controllers/User.controller'
import * as TransactionController from './controllers/Transaction.controller'

const guest = {name: "Guest"}

/**
 * Define the route to your controller 
 */
router.get('/', (req, res) => res.render('index.ejs', {user: req.user || guest}))

router.get('/register', (req, res) => res.render('register.ejs', {user: req.user || guest}))
router.get('/login', (req, res) => res.render('login.ejs', {user: req.user || guest}))

router.post('/api/register', registerNewUser)
router.post('/api/login', login)
router.get('/api/user/session', getSessionData)
router.get('/api/all-accounts', getAllAccount)
router.get('/api/all-sellers', getSellerList)
router.get('/api/my-account', TransactionController.getAccountBalance)
router.get('/products', (req, res) => res.render('products.ejs', {user: req.user || guest}))

//
router.post('/api/create-product', TransactionController.addProduct)
router.post('/api/buy-product', TransactionController.buyProduct)
router.get('/api/get-products', TransactionController.getProductList)
router.post('/api/deploy-seller-contract', TransactionController.deploySellerContract)

export default router.default
