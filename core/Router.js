import { Router } from 'express'

let router = new Router()
let routesTable = {}
const saveRoutesTable = (pattern, routeName) => {
  if (typeof routeName === 'string') {
    routesTable[routeName] = pattern
  }
}

export default {
  get: (pattern, callback, routeName) => {
    saveRoutesTable(pattern, routeName)
    router.get(pattern, callback)
    return this
  },
  post: (pattern, callback, routeName) => {
    saveRoutesTable(pattern, routeName)
    router.post(pattern, callback)
    return this
  },
  delete: (pattern, callback, routeName) => {
    saveRoutesTable(pattern, routeName)
    router.post(pattern, callback)
    return this
  },
  put: (pattern, callback, routeName) => {
    saveRoutesTable(pattern, routeName)
    router.put(pattern, callback)
    return this
  },
  routesTable,
  default: router
}
