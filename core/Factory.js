import { Strategy as LocalStrategy } from 'passport-local'
import UserModel from '../models/User'

export const InitPassPort = (passport) => {
  passport.use(
    'local',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password'
      },
      function(email, password, cb) {
        let field = 'email'
        if (!email.includes('@')) {
          field = 'username'
        }
        console.log(field)
        return UserModel.query()
          .findOne({ [field]: email })
          .then(async user => {
            if (!user || !(await user.verifyPassword(password))) {
              return cb(null, false, { message: 'Wrong password' })
            }
            return cb(null, user, {
              message: 'Logged In Successfully'
            })
          })
          .catch(err => {
            return cb(null, false, { message: err.message })
          })
      }
    )
  )
  // Put any user's information that frequently used on application controller
  passport.serializeUser( (user, done) => {
    var sessionUser = { id: user.id, fullName: user.fullName, email: user.email, role: user.role, account_address: user.account_address }
    done(null, sessionUser)
  })
  
  passport.deserializeUser( (sessionUser, done) => {
    done(null, sessionUser)
  })
}
