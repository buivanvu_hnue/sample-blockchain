const Knex = require('knex')
const knex = Knex(require('./knexfile'))
const uuidv1 = require('uuid/v1');
const Web3 = require('web3')
const Tx = require('ethereumjs-tx').Transaction
const contractObject = require('./contract/Marketplace.compiled.json')
const config = require("./config")
const web3 = new Web3(config.eth_http_provider)

// Poll event
let contractBlockFetched = {}

async function MainWorker() {
  const transactions = await knex("transactions").where({ 'type': 'deploy_contract' }).select()
  const contracts = []
  for (let item of transactions) {
    if (!item.contractAddress) continue
    const contract = new web3.eth.Contract(contractObject.abi, item.contractAddress)
    contracts.push(contract)
    // Subscribe to event
  }
  // Poll event
  function fetchEvent() {
    contracts.forEach(contract => {
      const fromBlock = contractBlockFetched[contract._address] ? Number(contractBlockFetched[contract._address]+1) : 0;
      console.log(`@Fetch Event from block ${fromBlock} - contract ${contract._address}`)
      contract.getPastEvents(
        'AllEvents',
        {
          fromBlock,
          toBlock: 'latest'
        },
        async (err, events) => {
          if (!err && events && events.length) {
            for (let ev of events) {
              try {
                await knex('events').insert({
                  'id': uuidv1(),
                  'eventName': ev.event,
                  'blockHash': ev.blockHash,
                  'blockNumber': ev.blockNumber,
                  'address': ev.address,
                  'transactionHash': ev.transactionHash || '',
                  'type': ev.type,
                  'event_id': ev.id,
                  'info': JSON.stringify(ev.returnValues),
                  'contractAddress': contract._address
                })
              }
              catch (err) {
                console.log(err)
              }
              // save fetched block number of this contract
              contractBlockFetched[contract._address] = ev.blockNumber
              console.log(ev.blockNumber)
            }
          }
        }
      )
    })
  }
  setInterval(fetchEvent, 10000) // run each 10 seconds
}

MainWorker()
  .then(console.log)
  .catch(console.log)

// // a list for saving subscribed event instances
// const subscribedEvents = {}
// // Subscriber method
// const subscribeLogEvent = (contract, eventName) => {
//   const eventJsonInterface = web3.utils._.find(
//     contract._jsonInterface,
//     o => o.name === eventName && o.type === 'event',
//   )
//   const subscription = web3.eth.subscribe('logs', {
//     address: contract.options.address,
//     topics: [eventJsonInterface.signature]
//   }, (error, result) => {
//     if (!error) {
//       const eventObj = web3.eth.abi.decodeLog(
//         eventJsonInterface.inputs,
//         result.data,
//         result.topics.slice(1)
//       )
//       console.log(`New ${eventName}!`, eventObj)
//     }
//   })
//   subscribedEvents[eventName] = subscription
// }