const defaultConfig = require("./default.json")
const dockerConfig = require("./docker-dev.json")
let config = {}
if (process.env.NODE_ENV == "development_docker") {
    config = Object.assign({}, defaultConfig, dockerConfig)
} else config = defaultConfig

module.exports = config